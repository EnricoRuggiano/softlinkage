import os
import cv2 as cv2
import numpy as np
import time
from matplotlib import pyplot as plt
from scipy.spatial import KDTree
import cupy as cp

from ..utils.utils import get_pts_in_column, fitline, distance_line_pts

def get_minimal_ring_samples(pts, h=2, k=10, number_of_samples=2, random=True, debug=True):
    start_time = time.time()
    
    tree_1 = KDTree(pts)
    tree_2 = KDTree(pts)
    
    sdm = tree_1.sparse_distance_matrix(tree_2, k)
    csr = sdm.tocsr()
    
    csr.data[csr.data<h] = 0
    csr.eliminate_zeros()
    
    groups = []
    for p in range(pts.shape[0]):
            tmp = np.argsort(csr.data[csr.indptr[p]:csr.indptr[p+1]])[:k - h]
            i = csr.indices[csr.indptr[p]:csr.indptr[p+1]][tmp]
            
            if isinstance(i, (list, np.ndarray)) and i.size == 0:
                continue
            
            if random:
                i = np.random.choice(i, size=number_of_samples - 1)
            else:
                groups.append((p, i[0]))
                continue
                
            if isinstance(i, (list, np.ndarray)):
                i = i.tolist()[::-1]
                i.append(p)
                i = i[::-1]
                groups.append(i)
            else:
                groups.append((p, i))

    # time utils
    end_time = time.time()
    elapsed = end_time - start_time
    if debug:
        print("===============================================")
        print("Minimal Ring Sample Completed! total time: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
        print("===============================================")        
    return np.array(groups)


def get_models_from_ring(pts, samples, d, debug=True):
    models = []
    
    start_time = time.time()
    for sample_j in samples:
        
        # get model
        coords = np.take(pts, sample_j, axis=0)
        coords_v  = get_pts_in_column(coords)
        line_j, _ = fitline(coords_v)

        # refine model
        ring_line_j, _ = refine_models(d, pts, [line_j], debug=False)

        models += ring_line_j
    
    # time utils
    end_time = time.time()
    elapsed = end_time - start_time
    if debug:
        print("===============================================")
        print("Refined Models of Minimal Ring Sample Completed! total time: {} - num of models {} - num of samples: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed)), len(models), len(samples)))
        print("===============================================")        
    
    models = np.array(models)
    return models

def refine_models(thr, pts, models, debug=True):
    start_time = time.time()
    new_models = []
    num_of_pts = []
    for m in models:
        d = distance_line_pts(pts, m)
        ind = np.argwhere(d<=thr).ravel()
        new_pts = np.take(pts, cp.asnumpy(ind), axis=0)
        num_of_pts.append(new_pts.shape[0])
        #print("num of pts extracted: {}".format(new_pts.shape))
        
        #new model
        new_pts_v = get_pts_in_column(new_pts)
        new_m, _ = fitline(new_pts_v)
        new_models.append(new_m)
    end_time = time.time()
    elapsed = end_time - start_time
    
    if debug:
        print("===============================================")
        print("Refining Models Completed! total time: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
        print("===============================================")        
        print("average num of points per model {}".format(np.mean(num_of_pts)))
    return new_models, num_of_pts