import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
import time
from matplotlib import pyplot as plt
import cupy as cp

from ..utils.utils import distance_line_pts, angle_between_lines, fitline, get_pts_in_column
from scipy.spatial import KDTree
from ..plot.plot import plot_lines

def get_cluster_distance_matrix(cluster_1, cluster_2, max_distance=5):
    kd_tree1 = KDTree(cluster_1)
    kd_tree2 = KDTree(cluster_2)
    sdm = kd_tree1.sparse_distance_matrix(kd_tree2, max_distance)
    sdm = sdm.toarray()
    
    # get border point of the clusters
    #border_idx = np.argwhere(sdm>0)
    borders_idx, _ = np.nonzero(sdm)
    borders_idx    = np.unique(borders_idx)
    borders = np.take(cluster_1, borders_idx, axis=0)
    
    return borders

def compare_clusters(cluster_1, cluster_2, cluster_dict, pts, max_distance=5, max_angled=10, debug=True, zoom_x=20, zoom_y=20):
    if cluster_1 == cluster_2:
        return False # same key don't merge
    
    line_1, _ = fitline(get_pts_in_column(cluster_dict[cluster_1]))
    line_2, _ = fitline(get_pts_in_column(cluster_dict[cluster_2]))
    
    tan_theta = angle_between_lines(line_1, line_2)
    theta = np.arctan(tan_theta) * 180 / np.pi
    
    borders = get_cluster_distance_matrix(cluster_dict[cluster_1], cluster_dict[cluster_2], max_distance)    
    are_mergable = np.abs(theta) < max_angled and borders.shape[0] > 0
    
    if debug:
        print("{} shape: {}".format(cluster_1, cluster_dict[cluster_1].shape))
        print("{} shape: {}".format(cluster_2, cluster_dict[cluster_2].shape))
        print("{} vs {} -> tan {} - atan {}".format(cluster_1, cluster_2, tan_theta, theta))
        print("{} vs {} -> {} near-points with distance low to {}".format(cluster_1, cluster_2, borders.shape[0], max_distance))
        
        _, ax = plt.subplots(figsize=(zoom_x, zoom_y))
        ax.set_xlim(0, max(pts[:, 0]))
        ax.set_ylim(0, max(pts[:, 1]))

        ax.axis('equal')
        ax.invert_yaxis()   
        ax.scatter(pts[:, 0], pts[:, 1])
        ax.scatter(cluster_dict[cluster_1][:, 0], cluster_dict[cluster_1][:, 1], c='r')
        ax.scatter(cluster_dict[cluster_2][:, 0], cluster_dict[cluster_2][:, 1], c='c')
        ax.scatter(borders[:, 0], borders[:, 1], c='y')
        
        plot_lines(pts, [line_1, line_2], ax=ax)
    return are_mergable

def merge_clusters(model_clusters, pts, max_distance, max_angled, debug=True):
    start_time = time.time()

    new_model_clusters = model_clusters.copy()

    for l_1 in model_clusters.keys():
        if l_1 not in new_model_clusters.keys():
            if debug:
                print("{} cluster already dropped".format(l_1))
            continue
    
        for l_2 in new_model_clusters.keys(): # aware of new cluster configurations
            merge = compare_clusters(l_1, l_2, new_model_clusters, pts, max_distance, max_angled, debug=False)
            if debug:
                print("{} vs {} -> {}".format(l_1, l_2, merge))
            if merge and l_1 != l_2:
                new_l_1_points = np.concatenate((new_model_clusters[l_1], new_model_clusters[l_2]), axis = 0)
                new_model_clusters.update({l_1: new_l_1_points})
                new_model_clusters.pop(l_2)
                
                if debug:
                    print("{} cluster dropped".format(l_2))
                    print("{} shape {} - {} shape {} - new {} shape {}".format(l_1, model_clusters[l_1].shape, l_2, model_clusters[l_2].shape, l_1, new_model_clusters[l_1].shape))
                break
    # time utils
    end_time = time.time()
    elapsed = end_time - start_time
    print("===============================================")
    print("Merged Clusters Completed! total time: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
    print("===============================================")
    return new_model_clusters