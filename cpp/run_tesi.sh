#!/bin/bash

# split filename removing .extension
IFS='.'
read -ra file <<< "$1"
length=${#file[@]}
file=${file[0]}
suffix=${file}

IFS=' '

echo $suffix
echo $1

./lens_distortion_correction_division_model_1p $1 out/${suffix}_canny.png out/${suffix}.png out/${suffix}_corrected_image.png 0.8 0.0 1.0 3.0 2.0 out/${suffix}.txt $2
