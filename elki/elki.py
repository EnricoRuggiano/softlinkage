from subprocess import call
import os
import time
from os import listdir
import re
import numpy as np

def logtime(func):
    def timed(*args, **kwarg):
        start_time = time.time()
        result = func(*args, **kwarg)
        elapsed = time.time() - start_time
        print("{}] took: {}".format(func.__name__, time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
        return result
    return timed
    
@logtime
def dump_precomputed_matrix(distance_matrix, out_path='src/java/distance_matrix.dat'):
    try:
        distance_matrix.dump(out_path)
        return out_path
    except Exception as e:
        print(e)
        return False

@logtime
def save_precomputed_matrix(distance_matrix, out_path='src/java/distance_matrix.dat'):
    try:
        with open(out_path, 'w') as f:
            for r in range(distance_matrix.shape[0]):
                for c in range(distance_matrix.shape[1]):
                    f.write("{} {} {}\n".format(r, c, distance_matrix[r, c]))
        f.close()
        return out_path
    except Exception as e:
        print(e)
        return False
    
def clean_path(out):
    cmd = """rm -r {}""".format(out)
    call(cmd.split())

def read_elkiCLUSTERS(pts, out_dir='src/java/dbscan', skip_lines = 4):
    
    # allign files/labels
    files = sorted([f for f in listdir(out_dir) if f != 'settings.txt' and f !='noise.txt'])
    labels = list(range(len(files)))
    
    # add noise cluster
    if 'noise.txt' in listdir(out_dir):
        files.insert(0, 'noise.txt')
        labels.insert(0, -1)
    #print(files)
    #print(labels)
    
    clusters = dict()
    f_indx   = 0
    for f in files:
        with open("{}/{}".format(out_dir, f), 'r') as f:
            content = f.readlines()[skip_lines:]
            content = np.array([ int(str.rstrip(c.replace("ID=", ""))) for c in content], dtype=int)
            #print(content)
            clusters.update({labels[f_indx]: np.take(pts, content, axis=0)})
        f.close()
        f_indx += 1
    return clusters



def elkiTEST():
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc.in java/mouse.csv 
        -algorithm clustering.kmeans.KMedoidsEM 
        -kmeans.k 3 
        -resulthandler ResultWriter 
        -out.gzip 
        -out java/output/k-3
        """
    call(cmd.split())
    
def elkiDBSCAN(distance_matrix, eps, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/dbscan'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.DBSCAN
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -dbscan.epsilon {}
        -dbscan.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), eps, min_samples, out)
    call(cmd.split())
    return out

def elkiLEADER(distance_matrix, eps, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/leader'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.Leader
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -leader.threshold {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), eps, out)
    call(cmd.split())
    return out


# GENERALIZED DBSCAN
def elkiGDBSCAN(distance_matrix, eps, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/gdbscan'
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.gdbscan.GeneralizedDBSCAN
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -dbscan.epsilon {}
        -dbscan.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), eps, min_samples, out)
    call(cmd.split())
    return out

def elkiLSDBC(distance_matrix, k, alpha=0.5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/lsdbc'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.gdbscan.LSDBC
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -lsdbc.k {}
        -lsdbc.alpha {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), k, alpha, out)
    call(cmd.split())
    return out

# OPTICS
def elkiOPTICSXi(distance_matrix, xi, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/opticsxi'
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.optics.OPTICSXi
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -opticsxi.xi {}
        -optics.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), xi, min_samples, out)
    call(cmd.split())
    return out

def elkiFastOPTICS(distance_matrix, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/fastoptics'
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.optics.FastOPTICS
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -optics.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), min_samples, out)
    call(cmd.split())
    return out

def elkiDeLiClu(distance_matrix, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/deliclu'
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.optics.DeLiClu
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -deliclu.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), min_samples, out)
    call(cmd.split())
    return out

def elkiOPTICSHeap(distance_matrix, eps, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/opticsheap'
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.optics.OPTICSHeap
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -optics.epsilon {}
        -optics.minpts {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), eps, min_samples, out)
    call(cmd.split())
    return out

#---------------------------------------------------------------------
# HDBSCAN

def elkiANDERBERG(distance_matrix, k, min_samples=5, linkage="WardLinkage", in_path='src/java/distance_matrix.dat'):
    out = 'src/java/anderberg'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm AnderbergHierarchicalClustering
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hierarchical.linkage {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), linkage, k, min_samples, out)
    call(cmd.split())
    return out

def elkiSLINK(distance_matrix, k, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/slink'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm SLINK
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), k, min_samples, out)
    call(cmd.split())
    return out

def elkiMINMAXANDERBERG(distance_matrix, k, min_samples=5, linkage="WardLinkage", in_path='src/java/distance_matrix.dat'):
    out = 'src/java/min_max_anderberg'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm MiniMaxAnderberg
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hierarchical.linkage {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), linkage, k, min_samples, out)
    call(cmd.split())
    return out

def elkiAGNES(distance_matrix, k, min_samples=5, linkage="WardLinkage", in_path='src/java/distance_matrix.dat'):
    out = 'src/java/agnes'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm AGNES
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hierarchical.linkage {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), linkage, k, min_samples, out)
    call(cmd.split())
    return out

def elkiCLINK(distance_matrix, k, min_samples=5, linkage="WardLinkage", in_path='src/java/distance_matrix.dat'):
    out = 'src/java/clink'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm CLINK
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hierarchical.linkage {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), linkage, k, min_samples, out)
    call(cmd.split())
    return out

def elkiHDBSCAN(distance_matrix, min_pts, k, min_samples=5,in_path='src/java/distance_matrix.dat'):
    out = 'src/java/hdbscan'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm HDBSCANLinearMemory
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hdbscan.minPts {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), min_pts, k, min_samples, out)
    call(cmd.split())
    return out

def elkiMINMAX(distance_matrix, k, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/min_max'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm MiniMax
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), k, min_samples, out)
    call(cmd.split())
    return out

def elkiMINMAXNNCHAIN(distance_matrix, k, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/min_max_nnchain'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm MiniMaxNNChain
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), k, min_samples, out)
    call(cmd.split())
    return out

def elkiNNCHAIN(distance_matrix, k, min_samples=5, linkage="WardLinkage", in_path='src/java/distance_matrix.dat'):
    out = 'src/java/nnchain'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm NNChain
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hierarchical.linkage {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), linkage, k, min_samples, out)
    call(cmd.split())
    return out

def elkiHDBSCANSLINK(distance_matrix, min_pts, k, min_samples=5, in_path='src/java/distance_matrix.dat'):
    out = 'src/java/hdbscanslink'
    # clean_path(out)
    cmd = """ java -jar src/java/elki-bundle-0.7.5.jar KDDCLIApplication 
        -dbc DBIDRangeDatabaseConnection
        -idgen.start 0
        -idgen.count {}
        -algorithm clustering.hierarchical.extraction.ClustersWithNoiseExtraction
        -algorithm SLINKHDBSCANLinearMemory
        -algorithm.distancefunction external.FileBasedSparseFloatDistanceFunction
        -distance.matrix {}
        -distance.parser AsciiDistanceParser
        -parser.colsep \s*[,;\s]\s*
        -parser.quote "'
        -string.comment ^\s*(#|//|;).*$
        -distance.default Infinity
        -hdbscan.minPts {}
        -extract.k {}
        -extract.minclsize {}
        -resulthandler ResultWriter 
        -out {}
        """.format(distance_matrix.shape[0], os.path.abspath(in_path), min_pts, k, min_samples, out)
    call(cmd.split())
    return out