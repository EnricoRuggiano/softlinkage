import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
from matplotlib import pyplot as plt

def get_camera_matrix(img):
    return np.vstack([ [1, 0, img.shape[1]/2], [0, 1, img.shape[0]/2], [0, 0, 1]])

def get_translation_matrix(img):
    return np.vstack([ [1, 0, img.shape[1]/2], [0, 1, img.shape[0]/2], [0, 0, 1]])

def distort_gt(img, pts, target_clusters, dist):
    camera_matrix = get_camera_matrix(img)
    
    # distort image
    out_img = cv2.undistort(img, camera_matrix, dist)
    
    # distort point dataset
    out_pts = pts.astype(float)
    out_pts = cv2.undistortPoints(out_pts, camera_matrix, dist, None, get_translation_matrix(img)).squeeze()
    
    # distort ground truth clusters
    out_target_clusters = dict()
    for k in list(target_clusters.keys()):
        cluster_pts = target_clusters[k].astype(float)
        new_pts = cv2.undistortPoints(cluster_pts, camera_matrix, dist, None, get_translation_matrix(img))
        new_pts = new_pts.squeeze()
        
        out_target_clusters.update({k:new_pts})

    return out_img, out_pts, out_target_clusters