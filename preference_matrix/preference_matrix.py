import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
import time
from matplotlib import pyplot as plt
import cupy as cp
from ..utils.utils import distance_line_pts

def get_preference_matrix(pts : np.array, models : np.array, roi_shape=(720,1280), limit=np.inf, step_debug=50, debug=True):
    
    # time utils
    start_time = time.time()
    
    x = pts[:, 0]
    y = pts[:, 1]
    matrix = cp.array([])
    
    step_time = time.time()
    i = 0
    for m in models:
        if i == limit:
            break
        
        if len(matrix) == 0 :
            matrix = distance_line_pts(pts, m, roi_shape=roi_shape)
        else:
            matrix = cp.column_stack([matrix, distance_line_pts(pts, m, roi_shape=roi_shape)])
        
        # step time end
        if i % step_debug == 0:
            end_time = time.time()
            elapsed = end_time - step_time
            
            if debug:
                print("{}] took: {}".format(i, time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
            step_time = time.time()
        i += 1
        
    # time utils
    end_time = time.time()
    elapsed = end_time - start_time
    
    if True:
        print("===============================================")
        print("Preference Matrix Completed! total time: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
        print("===============================================")
    return matrix

def print_matrix_info(matrix):
    mean    = matrix[matrix < np.inf].mean()
    minimum = np.min(matrix[matrix < np.inf])
    maximum = np.max(matrix[matrix < np.inf])
    
    num_inf = np.sum((matrix==np.inf).ravel())
    num_min = np.sum((matrix == minimum).ravel())
    num_max = np.sum((matrix == maximum).ravel())
    
    below_avg = np.sum((matrix < mean).ravel())
    above_avg = np.sum((matrix > mean).ravel()) -  num_inf
    
    print("mean:\t {}".format(mean))
    print("min:\t {}".format(minimum))
    print("max:\t {}".format(maximum))
    print("num of total:\t {}".format(matrix.shape[0]))
    print("num of dropped:\t {}".format(num_inf))
    print("num of minimum:\t {}".format(num_min))
    print("num of maximum:\t {}".format(num_max))
    print("num of < average:\t {}".format(below_avg))
    print("num of > average:\t {}".format(above_avg))
    
def print_model_info_matrix(matrix, model_i):
    col = matrix[:, model_i]
    if len(col) == 0:
        print("{} model] pts: 0 num of min: 0, num of max: 0 min: np.inf, max: np.inf".format(model_i))
        return
    num_pts = np.sum((col < np.inf).ravel())

    mean    = col[col < np.inf].mean()
    minimum = np.min(col[col < np.inf])
    maximum = np.max(col[col < np.inf])
    
    num_min = np.sum((col == minimum).ravel())
    num_max = np.sum((col == maximum).ravel())

    print("{} model] pts: {} num of min: {}, num of max: {} min: {}, max: {}".format(model_i, num_pts, num_min, num_max, minimum, maximum))

def model_num_pts(col):
    return np.sum((col < np.inf).ravel())

def model_top_ten(matrix):
    return np.argsort(np.apply_along_axis(model_num_pts, 0, matrix))[::-1][:10]    