import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import time
from scipy.spatial import KDTree
from subprocess import call
import re
import math
from matplotlib import pyplot as plt
import cupy as cp


def nemesis_input_image(dis_pts, img):
    height, width = img.shape
    nemesis_img = np.zeros(shape=(height, width, 3), dtype=np.uint8)
    nemesis_img[dis_pts[:, 1], dis_pts[:, 0]] = (255, 255, 255)
    return nemesis_img

def nemesis_save_input_image(dis_pts, dis_target_clusters, img, img_path):
    nemesis_img = nemesis_input_image(dis_pts, img)
    tmp_path = 'src/cpp/{}_eval.png'.format(img_path)
    cv2.imwrite(tmp_path, nemesis_img)
    return tmp_path

# run nemesi
def runNemesi(input_path, n_lines=100):
    name = input_path.split('/')[1].split('.')[0]
    output_path = 'src/cpp/out/{}.txt'.format(name)
    
    cmd = """./src/cpp/lens_distortion_correction_division_model_1p 
             {} 
             src/cpp/out/{}_canny.png 
             src/cpp/out/{}.png 
             src/cpp/out/{}_corrected_image.png 
             0.8 
             0.0 
             1.0 
             3.0
             2.0 
             {}
             {}
        """.format(input_path, name, name, name, output_path, n_lines)
    call(cmd.split())
    print('nemesi output file is saved at {}'.format(output_path))
    return output_path

def getNemesiClusters(output_path, img, debug=False):
    height, width = img.shape
    
    with open(output_path, 'r') as f:
        content = f.read().splitlines()
    nemesi_clusters = dict()

    # drop top 6 lines
    header = 6
    num_lines = int(re.search(r'\d+', content[header]).group(0))
    if debug:
        print("num of lines: {}".format(num_lines))

    lines = content[header+1:]
    lines_headers = 4
    for i in range(num_lines):
        clusters_pts = []

        lines_pts = int(lines[lines_headers])
        lines = lines[lines_headers + 1:]
        
        if debug:
            print("cluster: {} num of pts: {} elements: {}".format(i, lines_pts, lines[:10]))
        for j in range(lines_pts):
            p = lines[j].split()
            clusters_pts.append([np.abs(0 - int(p[0])), np.abs(height - int(p[1]))])    
            #clusters_pts.append([int(ps) for ps in p])
            
        #update cluster dict
        nemesi_clusters.update({i: np.array(clusters_pts)})

        # scale lines
        lines = lines[lines_pts:]
    return nemesi_clusters

def pt2d_in_set(point, target_set):
    is_present = target_set == point # e.g: dis_pts == [112, 385]
    truth = any(np.bitwise_and(is_present[:, 0], is_present[:, 1]))
    return truth
    
def check_clusters_consistency(nemesi_clusters, dis_pts):
    global_is_valid = True
    for k in nemesi_clusters.keys():
        is_valid_pts = [pt2d_in_set(x, dis_pts) for x in nemesi_clusters[k]]
        is_valid = all(is_valid_pts)
        if not is_valid:
            print("cluster {} is not valid. wrong pts: {}".format(k, np.argwhere(is_valid_pts)))
            global_is_valid = False
    return global_is_valid

def fix_clusters_pts_with_nearest(nemesi_clusters, dis_pts, debug=False):
    keys = nemesi_clusters.keys()
    tree = KDTree(dis_pts)
    new_nemesi_clusters = dict()
    for k in keys:
        if debug:
            print("{}] cluster".format(k))
        to_fix = nemesi_clusters[k]
        fixed = []
        for p in to_fix: # for all pts of nemesi cluster[key]
            for d in np.arange(0, 11):  # check distances from 0 to 10 (0 = point is present)
                idx = tree.query_ball_point(p, d)
                if len(idx) > 0:
                    found = dis_pts[idx[0]]
                    fixed.append(found)
                    if debug:
                        print("{} -> mapped to {} with distance {}".format(p, found, d))
                    break # go to next point
        fixed = np.array(fixed)
        new_nemesi_clusters.update({k:fixed})
    return new_nemesi_clusters

# get nemesi noise cluster
def get_target_noise_cluster(pts, target_clusters):
    target_clusters_pts = []
    for v in target_clusters.values():
        if len(target_clusters_pts) == 0:
            target_clusters_pts = v
        else:
            #print(target_clusters_pts)
            #print(v)
            target_clusters_pts = np.concatenate((target_clusters_pts, v), axis = 0)

    target_clusters_pts = np.unique(target_clusters_pts, axis = 0)
    target_clusters_pts = [set(x) for x in target_clusters_pts]
    noise_pts = []
    for p in pts:
        if set(p) not in target_clusters_pts:
            noise_pts.append(p)
    noise_pts = np.array(noise_pts)
    print("tot pts: {} - noise pts: {} + cluster pts: {} - sum: {}".format(
        pts.shape[0], noise_pts.shape[0], len(target_clusters_pts), noise_pts.shape[0] + len(target_clusters_pts)))
    return noise_pts