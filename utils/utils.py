import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
from matplotlib import pyplot as plt
import cupy as cp

def get_pts(img):
    row, col = img.nonzero()
    pts = np.column_stack((col, row)) # this are the points
    return pts

def to_vertical(pts):
    return get_pts_in_column(pts)

def multidim_intersect(a, b):
    return np.array([x for x in set(tuple(x) for x in a) & set(tuple(x) for x in b)])

def get_pts_in_column(pts):
    new_pts = pts.copy()
    new_pts = new_pts.astype(float)
    new_pts = new_pts.T
    return new_pts

def discretize_dataset(pts, target_clusters):
    dis_pts = np.unique(pts.astype(int), axis=0)
    dis_target_clusters = dict()
    for k in target_clusters.keys():
        v = np.unique(target_clusters[k].astype(int), axis=0)
        dis_target_clusters.update({k: v})
    return dis_pts, dis_target_clusters

def count_repeated_elements(array, debug=False):
    array_sorted = array[array[:,0].argsort(axis=0).ravel()]
    prev = [0, 0, 0]
    i = 0
    num = 0
    for elem in array_sorted:
        if np.array_equal(elem, prev):#elem[0][0] == prev[0][0] and elem[0][1] == prev[0][1] and elem[0][2] == prev[0][2]:
            if debug:
                print("{}] already found {}".format(i, elem))
            num += 1
        prev = elem
        i += 1
    return num

# points are columns as row_1 = x, row_2 = y
def fitline(XY):
    
    (rows, npts) = XY.shape
    
    if npts < 2:
        print('Too few points to fit line')
    
    if rows == 2: # add jp,pgemepis scale coordinate of 1
        XY = np.vstack((XY, np.ones(shape=(1, npts))))
    
    if npts == 2: # Pad XY with a third column of zeros
        XY = np.hstack((XY, np.zeros(shape=(3, 1))))
 
    # normalize
    (XYn, T) = normalise2dpts(XY)
    
    # svd
    u, d, vh = np.linalg.svd(XYn.conj().T)
    v = vh.T
    
    C = v[:, 2]
    
    # denormalize solution
    C = np.dot(T.conj().T, C)
    
    # rescale
    C = C / np.sqrt(C[0]**2 + C[1]**2)
    
    dist = np.abs(np.dot(C[0], XY[0, :]) + np.dot(C[1], XY[1, :]) + C[2])

    return C, dist

def normalise2dpts(pts):
    if pts.shape[0] < 3:
        print('pts must be 3xN')
        return
    
    # find the indices of points that are not at infinity
    finiteind = np.where(np.abs(pts[2, :]) > np.finfo(float).eps)
    
    # ensure homogeneous coords
    pts[0, finiteind] = pts[0, finiteind] / pts[2, finiteind]
    pts[1, finiteind] = pts[1, finiteind] / pts[2, finiteind]
    pts[2, finiteind] = 1
    
    c = np.zeros(shape=2)
    c[0] = np.mean(pts[0, finiteind])
    c[1] = np.mean(pts[1, finiteind])

    newp = np.zeros(shape=pts.shape)
    newp[0, finiteind] = pts[0, finiteind] - c[0]
    newp[1, finiteind] = pts[1, finiteind] - c[1]
    
    dist = np.sqrt(newp[0, finiteind]**2 + newp[1, finiteind]**2)
    meandist = np.mean(dist[:]) # ensure dist is a column vetor for octave

    scale = np.sqrt(2) / meandist
    
    T = np.array([[scale, 0, -scale*c[0]], [0, scale, -scale*c[1]], [0, 0, 1]])
    
    newpts = np.dot(T,  pts)
    return newpts, T

def distance_line_pts(pts, line, roi_shape=(720,1280)):

    x = cp.array(pts[:, 0])
    y = cp.array(pts[:, 1])

    a, b, c = line
    qs = cp.array(np.repeat(c, len(x)))
    
    #d = np.abs((np.multiply(C[0], x) + np.multiply(C[1], y) + C[2]) / np.sqrt(C[0]**2 + C[1]**2))

    d = np.abs((cp.multiply(a, x) + cp.multiply(b, y) + qs) / cp.sqrt(a**2 + b**2))
    #d[d>np.min(roi_shape)] = np.inf
    return d

def angle_between_lines(line_1, line_2):
    # tan theta = (a2b1 - a1b2) / (a1a2 + b1b2)
    
    a1 = line_1[0]
    b1 = line_1[1]

    a2 = line_2[0]
    b2 = line_2[1]
    
    tan_theta = (a2*b1 - a1*b2) / (a1*a2 + b1*b2 )
#    theta = np.arctan(tan_theta)
    return tan_theta #theta