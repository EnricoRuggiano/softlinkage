import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
from matplotlib import pyplot as plt
import cupy as cp

def allineate_keys_dict(dictionary):
    new_dict = dict()
    i = 0
    
    for k in dictionary.keys():
        new_dict.update({i: dictionary[k]})
        i += 1
    return new_dict

def count_p_in_cluster(point, cluster, debug=True):
    size = len(cluster.items())
    count = np.zeros(shape=size, dtype=int)
    for k, v in cluster.items():
        found = set(point) in [set(x) for x in v]
        if found:
            count[k] += 1 
            if debug:
                print("{} in cluster {}: {}".format(p, k, found))
    return count

def model2target_labels(model_clusters, target_clusters):
    m2t_labels = dict()
    for k in model_clusters.keys():
        result = similarity_cluster_matrix(model_clusters[k], target_clusters)
        m2t_labels.update({k: (np.argmax(result), np.max(result))})
    return m2t_labels

def similarity_cluster_matrix(cluster, target_cluster):
    score = np.zeros(shape=len(target_cluster.keys()), dtype=int)
    for p in cluster:
        score = np.add(score, count_p_in_cluster(p, target_cluster, debug=False))
    return score

def clean_dict(dictionary, debug=False):
    l = sorted(dictionary.items(), key=lambda kv: kv[1], reverse=True)
    dictionary_clean = {}
    already_used_key = []
    for elem in l:
        k = elem[0]
        v, count = elem[1]
        if debug:
            print("k- {} - v {} present: {}".format(k, v, v in already_used_key))
        if v in already_used_key:
            dictionary_clean.update({k: "WRONG"})
        else:
            already_used_key.append(v)
            dictionary_clean.update({k: v})
    # sort by key
    dictionary_clean = {x[0]: x[1] for x in sorted(dictionary_clean.items(), key=lambda kv: kv[0])}
    return dictionary_clean

def get_point_cluster_label(point, cluster, debug=True):
    size = len(cluster.items())
    for k in cluster.keys():
        found = set(point) in [set(x) for x in cluster[k]]
        if found:
            return k
            if debug:
                print("{} in cluster {}: {}".format(point, k, found))
    return "NOISE"

def missclassification_error(pts, model_clusters, target_clusters, t2m_dict):
    num_pts = pts.shape[0]
    errors = 0
    for p in pts:
        model_label  = get_point_cluster_label(p, model_clusters)
        target_label = get_point_cluster_label(p, target_clusters)
        errors += t2m_dict[target_label] == model_label
    return errors/num_pts * 100