import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
import time
from matplotlib import pyplot as plt
import cupy as cp

from scipy.spatial.distance import jaccard, pdist, squareform
from sklearn.metrics import pairwise_distances

def get_distance_matrix_multithreaded(matrix, thr):
    start_time = time.time()
    
    truncated = cp.zeros(shape=matrix.shape,dtype=np.uint8)
    truncated[matrix<thr] = 1
    
    # calculate distance between rows
    truncated = cp.asnumpy(truncated)
    distance_matrix = pairwise_distances(truncated, metric='jaccard', n_jobs=-1)
    #distance_matrix = squareform(distance_matrix)

    # time utils
    end_time = time.time()
    elapsed = end_time - start_time
    print("===============================================")
    print("Distance Matrix Completed! total time: {}".format(time.strftime("%H h %M m %S s", time.gmtime(elapsed))))
    print("===============================================")
    return distance_matrix