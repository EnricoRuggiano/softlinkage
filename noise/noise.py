import os
import cv2 as cv2
import numpy as np
from matplotlib import pyplot as plt

def add_noise(pts, noise_quantity=0.1):
    noise = np.random.randint(low=(0, 0), high=(max(pts[:, 0]), max(pts[:, 1])), size=(int(pts.shape[0]*noise_quantity), 2))
    print("Adding pts noise: {}% ( {} + {})".format(int(noise.shape[0]/pts.shape[0]*100), noise.shape[0], pts.shape[0]))
    pts = np.vstack([pts, noise])
    return pts, noise