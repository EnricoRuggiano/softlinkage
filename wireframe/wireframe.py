import os
import cv2 as cv2
import numpy as np
import json  as json
import imutils
import math
from matplotlib import pyplot as plt

import pickle

def load_data(pkl_path, base_dir ='./dataset'):
    data_file = open(base_dir+"/"+pkl_path, 'rb')
    data = pickle.load(data_file)
    data_file.close()
    return data

def plot_data_lines(path):
    d = load_data(path)
    lines = d['lines']
    im = d['img']
    points = d['points']
    
    fig, ax = plt.subplots(2,1, figsize=(20, 20))
    ax[0].imshow(d['img'])
    for idx, (i, j) in enumerate(lines, start=0):
        x1, y1 = points[i]
        x2, y2 = points[j]
        cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2, cv2.LINE_8)
        ax[1].scatter([x1, x2], [y1, y2], c='b')

    plt.axis('equal')
    plt.gca().invert_yaxis()
    ax[1].imshow(im)

def plot_data_lines_distortion(path, dist):
    d = load_data(path)
    lines = d['lines']
    im = d['img']
    points = d['points']
    
    camera_matrix = np.vstack([ [1, 0, im.shape[1]/2], [0, 1, im.shape[0]/2], [0, 0, 1]])
    fig, ax = plt.subplots(1,1, figsize=(20, 20))
    for idx, (i, j) in enumerate(lines, start=0):        
        x1, y1 = points[i]
        x2, y2 = points[j]
        cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2, cv2.LINE_8)

    plt.axis('equal')
    plt.gca().invert_yaxis()
    ax.imshow(cv2.undistort(im, camera_matrix, dist))


def get_mask(path, stroke=1):
    d = load_data(path)
    points = d['points']
    lines  = d['lines']
    img    = d['img']
    
    target_clusters = dict()
    mask = np.zeros(shape=(img.shape[0], img.shape[1]), dtype=np.uint8)

    for idx, (i, j) in enumerate(lines, start=0):
        x1, y1 = points[i]
        x2, y2 = points[j]

        cv2.line(mask, (int(x1), int(y1)), (int(x2), int(y2)), (255, 255, 255), stroke, cv2.LINE_8)
        curr_line = np.zeros(shape=(img.shape[0], img.shape[1]), dtype=np.uint8)

        # add to cluster
        cv2.line(curr_line, (int(x1), int(y1)), (int(x2), int(y2)), (255, 255, 255), stroke, cv2.LINE_8)
        line_pts_x, line_pts_y = np.nonzero(curr_line)
        line_pts = np.vstack([line_pts_y, line_pts_x]).transpose()
        target_clusters.update({idx:line_pts})
    
    return mask, target_clusters

