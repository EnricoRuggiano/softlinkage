import os
import cv2 as cv2
import numpy as np
from matplotlib import pyplot as plt

from .wireframe.wireframe import *
from .utils.utils import * 
from .noise.noise import *
from .plot.plot   import *
from .radial_distortion.radial_distortion import *
from .minimum_sample_set.minimum_sample_set import *
from .preference_matrix.preference_matrix import *
from .distance_matrix.distance_matrix import *
from .elki.elki import *
from .post_processing.post_processing import *
from .missclassification_error.missclassification_error import *
from .nemesi.nemesi import *


def soft_linkage(
    img_path,
    sample_rings,
    alpha,      # sample ring inlier threshold
    gamma,      # preference threshold
    epsilon,    # density based cluster neighboors threshold
    minPts,     # density based cluster minimun number pts 
    algorithm,  # elki cluster algorithm
    eta,        # post processing: distance between clusters
    beta,       # post processing: angle threshold between clusters model
    qty_noise = 0.1,
    dist =  np.array([1e-05, 0, 0, 0]), # k1, k2, p1, p2
    ):

    #--------------------------
    # Carica Immagine dal dataset Wireframe
    img = load_data(img_path)['img']
    pts = load_data(img_path)['points']
    pts = np.array(pts, dtype=int)
    plot_data_lines(img_path)

    #--------------------------
    # dati spazio rettilineo
    # get image
    img, target_clusters = get_mask(img_path, stroke=1)

    ## Get Pts
    pts = get_pts(img)
    pts, noise = add_noise(pts, qty_noise)
    target_clusters[-1] = noise # add noise

    plt.figure(figsize=(20,20))
    plot_target_cluster_pts(target_clusters[8], pts)

    # backup data
    or_img = img.copy()
    or_pts = pts.copy()
    or_target_clusters = target_clusters.copy()

    # check consistency dataset gt -> clusters gt
    assert(target_clusters[0].shape == multidim_intersect(pts, target_clusters[0]).shape)

    #--------------------------
    # dati spazio non-lineare
    # show input dataset ground truth
    demo_img, demo_pts, _ = distort_gt(img, get_pts(img), target_clusters, dist)
    plot_data_lines_distortion(img_path, dist)

    # get distorted data
    img, pts, target_clusters = distort_gt(img, pts, target_clusters, dist)

    # Discretize the dataset
    pts, target_clusters = discretize_dataset(pts, target_clusters)

    #target_clusters = add_noise_cluster(img, noise, target_clusters, dist)
    plot_target_cluster_pts(target_clusters[8], pts)

    ## Get Pts
    print(pts.shape)

    # check consistency dataset gt -> clusters gt
    assert(target_clusters[0].shape == multidim_intersect(pts, target_clusters[0]).shape)

    #--------------------------
    ## 1- Calcola modelli

    models = []

    for h, k in sample_rings:
        print("\n***********************************************")
        print("Sample ring: {}-{}".format(h, k))
        print("***********************************************")
        
        samples = get_minimal_ring_samples(pts, h, k, number_of_samples=2)
        ring_models  = get_models_from_ring(pts, samples, alpha)
        
        # check unique models
        num_unique_ms = np.unique(ring_models, axis=0).shape[0]
        assert(num_unique_ms == (ring_models.shape[0] -  count_repeated_elements(ring_models)))
        print("Total models: {} - unique models: {}".format(len(ring_models), num_unique_ms))
        ring_models = np.unique(ring_models, axis=0)
        
        if len(models) == 0:
            models = ring_models
        else:
            models = np.concatenate((ring_models, models), axis=0)
        #num_new_pts += num_new_pts_tmp
        
    models = np.unique(models, axis=0)
    print("\n***********************************************")
    print("Multi Sample-Ring Models numbers: {}".format(models.shape[0]))
    print("***********************************************")

    #--------------------------
    ### 2 - Calcola la preference matrix
    matrix = get_preference_matrix(pts, models, roi_shape=img.shape, step_debug=1000, debug=False)

    #--------------------------
    ### 3 - Calcola la distance matrix

    distance_matrix = get_distance_matrix_multithreaded(matrix, gamma)
    save_precomputed_matrix(distance_matrix)

    #--------------------------
    ### 3 - Performa un algoritmo di clustering density-based

    if algorithm == 'DBSCAN':
        out_path = elkiDBSCAN(distance_matrix, epsilon, minPts)
    elif algorithm == 'HDBSCAN':
        out_path = elkiHDBSCAN(distance_matrix, minPts, epsilon, minPts)
    elif algorithm == 'HDBSCAN SLINK':
        out_path = elkiHDBSCANSLINK(distance_matrix, minPts, epsilon, minPts)
    elif algorithm == 'SLINK':
        out_path = elkiSLINK(distance_matrix, epsilon, minPts)
    elif algorithm == 'ANDERBERG':
        out_path = elkiANDERBERG(distance_matrix, epsilon, minPts)
    elif algorithm == 'NNCHAIN':
        out_path = elkiNNCHAIN(distance_matrix, epsilon, minPts)
    else:
        raise Exception("no density cluster algorithm provided")
    
    model_clusters = read_elkiCLUSTERS(pts, out_path)
    print_output_no_roi(model_clusters, pts, are_indices=False, white=True)

    #--------------------------
    ### 4 - Performa Post Processing

    noise_cluster = model_clusters.pop(-1)
    new_model_clusters = merge_clusters(model_clusters, pts, eta, beta, debug=False)
    new_model_clusters = allineate_keys_dict(new_model_clusters)
    new_model_clusters.update({-1: noise_cluster})

    print_output_no_roi(new_model_clusters, pts, white=True, are_indices=False)

    #--------------------------
    ### Calcola il missclassification error
    target_noise_cluster = target_clusters.pop(-1)
    noise_cluster = new_model_clusters.pop(-1)

    # add the dictionary to allineate the cluster labels
    t2m_dict = model2target_labels(target_clusters, new_model_clusters)
    t2m_dict = clean_dict(t2m_dict)

    # add the noise
    new_model_clusters.update({-1: noise_cluster})
    target_clusters.update({-1: target_noise_cluster})
    t2m_dict.update({-1: -1})

    # calculate the error
    mce = missclassification_error(pts, new_model_clusters, target_clusters, t2m_dict)
    print("MCE: {}".format(mce))
    return mce