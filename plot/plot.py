import os
import cv2 as cv2
import numpy as np
from matplotlib import pyplot as plt

from ..utils.utils import get_pts_in_column, fitline, distance_line_pts
from ..minimum_sample_set.minimum_sample_set import refine_models


def plot_target_cluster_pts(pts, gt_pts, color='r'):
    _, ax = plt.subplots(figsize=(20, 20))
    
    plt.axis('equal')
    plt.gca().invert_yaxis()    
    
    ax.scatter(gt_pts[:, 0], gt_pts[:, 1], c='g')
    ax.scatter(pts[:, 0], pts[:, 1], c=color)#, alpha=0.2)


def plot_ring(pts, group, h=2, k=10, thr = 3, zoom_x=20, zoom_y=20):
    _, ax = plt.subplots(figsize=(zoom_x, zoom_y))

    #ax.set_xlim(0, 20)
    #ax.set_ylim(0, 20)        
    
    ax.set_xlim(0, max(pts[:, 0]))
    ax.set_ylim(0, max(pts[:, 1]))

    ax.axis('equal')
    ax.invert_yaxis()   
    
    p = pts[group[0]]
    s = pts[group[1]]
    print(p)
    print(s)
    
    ax.scatter(pts[:, 0], pts[:, 1])
    ax.scatter(p[0], p[1], c='blue')
    ax.scatter(s[0], s[1], c='red')

    circle_in = plt.Circle((p[0], p[1]), h, color='r', fill=True, alpha=0.5)
    circle_out = plt.Circle((p[0], p[1]), k, color='r', fill=True, alpha=0.25)
    ax.add_patch(circle_in)
    ax.add_patch(circle_out)
    
    # print line
    pts_ring_v  = get_pts_in_column(np.array([p, s]))
    line_j, _ = fitline(pts_ring_v)
    print_model_with_thr(line_j, thr, ax, xmax=max(pts[:, 0]), ymax=max(pts[:, 1]), color='b')

    # print refined line
    ring_models, _ = refine_models(thr, pts, [line_j])
    print_model_with_thr(ring_models[0], 0, ax, xmax=max(pts[:, 0]), ymax=max(pts[:, 1]), color='r')
    

def print_model_with_thr(coef, thr, ax, xmax=100, ymax=100, color='r', alpha=1):
    xs = np.linspace(0 , xmax, xmax)
    C = coef
    
    if C[0] == 0.0: # horizontal line ys = C[2]/C[1]
        ax.axhline(y=-C[2]/C[1], xmin=0, xmax=xmax, color=color, alpha=alpha)
    
    elif C[1] == 0.0: # vertical line 0 = -C[0]xs - C[2]
        ax.axvline(x=-C[2]/C[0], ymin=0, ymax=ymax, color=color, alpha=alpha)
    else:
        ys = -(C[0] * xs + C[2])/C[1] # c[0] * x + c[1] * y + C[2] => y = (- C[2] - C[0] * x ) / c[1]
        
        axes = plt.gca()
        axes.set_xlim([0,xmax])
        axes.set_ylim([0,ymax])
        axes.invert_yaxis()
        
        ax.plot(xs, ys, color, alpha=alpha)
        
        old_xs = xs
        ax.fill_betweenx(ys, xs-np.sqrt(thr), xs+np.sqrt(thr), facecolor='b',alpha=0.2)

import matplotlib.colors as colors
import random
base_colors = list(colors.BASE_COLORS)
del base_colors[2]
other_colors = list(colors.cnames.keys())
random.shuffle(other_colors)

all_colors = base_colors + other_colors

def print_output_no_roi(clusters, pts, white=True, d=None, are_indices=True):
    cluster_labels = list(clusters.keys())

    fig, ax = plt.subplots(figsize=(20, 20))
    plt.axis('equal')
    plt.gca().invert_yaxis()
    #plt.title(label)
    
    for i in range(len(cluster_labels)):
        l = cluster_labels[i]

        if i >= len(all_colors):
            color = all_colors[i%len(all_colors)]
        else:
            color = all_colors[i] if l !=-1 else 'r'
        
        if are_indices:
            coords = np.take(pts, clusters[l], axis=0)
        else:
            coords = clusters[l]
            
        if white is False:
            ax.set_facecolor('black')
        
        ax.scatter(coords[:, 0], coords[:, 1], 1, c=color)
        if d is not None:
            ax.errorbar(coords[:, 0], coords[:, 1],  fmt='o', color=color, yerr=d, xerr=d, alpha=0.1)# elinewidth=0.5, fmt='o', ecolor=color)

def plot_lines(pts, line_1, line_2, ax=None, zoom_x=20, zoom_y=20):
    if ax is None:
        _, ax = plt.subplots(figsize=(zoom_x, zoom_y))
        ax.set_xlim(0, max(pts[:, 0]))
        ax.set_ylim(0, max(pts[:, 1]))

        ax.axis('equal')
        ax.invert_yaxis()   
        ax.scatter(pts[:, 0], pts[:, 1])
    print_model_with_thr(line_1, 0, ax, xmax=max(pts[:, 0]), ymax=max(pts[:, 1]), color='r')
    print_model_with_thr(line_2, 0, ax, xmax=max(pts[:, 0]), ymax=max(pts[:, 1]), color='c')
